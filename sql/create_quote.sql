CREATE TABLE `conygre`.`quotes` (
                                    id int primary key auto_increment,
                                    customer_id int not null,
                                    policy_number bigint not null,
                                    price int not null,
                                    email varchar(50) not null,
                                    start_date date not null,
                                    end_date date not null,
                                    vin VARCHAR(17) not null,
                                    date_created timestamp default now(),
                                    foreign key (customer_id) REFERENCES customers(id)
);

INSERT INTO `conygre`.`quotes` (`policy_number`, `customer_id`, `price`, `email`, `start_date`, `end_date`, `vin`) VALUES ('29384342', '1', '1200', 'johndoe@gmail.com', '2021-03-04', '2021-12-04', '1GKET16S426107309');
INSERT INTO `conygre`.`quotes` (`policy_number`, `customer_id`, `price`, `email`, `start_date`, `end_date`, `vin`) VALUES ('4873945', '2', '1400', 'janedoe@gmail.com', '2021-12-08', '2022-04-04', '1HD1FRW1X4Y713274');
