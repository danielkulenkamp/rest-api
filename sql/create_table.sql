CREATE TABLE `conygre`.`customers` (
                                       `id` INT NOT NULL AUTO_INCREMENT,
                                       `first_name` VARCHAR(45) NOT NULL,
                                       `last_name` VARCHAR(45) NOT NULL,
                                       `city` VARCHAR(45) NOT NULL,
                                       `zip_code` CHAR(5) NOT NULL,
                                       `date` DATE,
                                       PRIMARY KEY (`id`));

INSERT INTO `conygre`.`customers` (`id`, `first_name`, `last_name`, `city`, `zip_code`, `date`) VALUES ('1', 'Daniel', 'Kulenkamp', 'Tempe', '85281', '2021-07-09');
INSERT INTO `conygre`.`customers` (`id`, `first_name`, `last_name`, `city`, `zip_code`, `date`) VALUES ('2', 'Jesus', 'Chavez', 'Charlotte', '28212', null);
