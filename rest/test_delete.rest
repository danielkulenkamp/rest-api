POST http://localhost:8080/api/customers/add
Content-Type: application/json

{
  "firstName": "Jimmy",
  "lastName": "Doe",
  "city": "Chicago",
  "zipCode": "12345",
  "date": "2021-07-09"
}

###
DELETE http://localhost:8080/api/customers/delete/201

###
DELETE http://localhost:8080/api/quotes/delete/4