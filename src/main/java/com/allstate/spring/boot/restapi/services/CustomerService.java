package com.allstate.spring.boot.restapi.services;

import com.allstate.spring.boot.restapi.entities.Customer;

import java.util.Collection;
import java.util.Optional;

public interface CustomerService {
    Collection<Customer> getAllCustomers();
    Optional<Customer> getCustomerById(int id);
    Collection<Customer> findCustomersByFirstName(String firstName);
    Collection<Customer> findCustomersByLastName(String lastName);
    Collection<Customer> findCustomersByFirstAndLastName(String firstName, String lastName);
    Collection<Customer> findCustomersByCity(String city);
    Collection<Customer> findCustomersByZipCode(String zip);
    Customer updateCustomer(Customer updatedCustomer);
    void deleteCustomer(Customer customer);
    Customer createCustomer(Customer newCustomer);
}
