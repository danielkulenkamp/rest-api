package com.allstate.spring.boot.restapi.repo;

import com.allstate.spring.boot.restapi.entities.Quote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface QuoteRepository extends JpaRepository <Quote, Integer> {

    //=============== quotes controller repo ==============================
    Optional<Quote> findQuoteById(int quoteId);
    List<Quote> findQuotesByCustomerId(int customerId);
//    List<Quote> findQuotesByPolicyNumber(int PolicyNumber);
//    List<Quote> findQuotesByPrice(String email);
//    List<Quote> findQuotesByStartDate(LocalDate startDate);
//    List<Quote> findQuotesByEndDate(LocalDate endDate);
    List<Quote> findQuotesByEmail(String email);
    List<Quote> findQuotesByVin(String vin);

}
