package com.allstate.spring.boot.restapi.repo;

import com.allstate.spring.boot.restapi.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository <Customer, Integer> {

    //=============== customer controller repo ==============================
    List<Customer> findCustomersByFirstName(String firstName);
    List<Customer> findCustomersByLastName(String lastName);
    List<Customer> findCustomersByFirstNameAndLastName(String firstName, String lastName);
    List<Customer> findCustomersByCity(String city);
    List<Customer> findCustomersByZipCode(String zipCode);
}
