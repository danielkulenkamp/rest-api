package com.allstate.spring.boot.restapi.services;

import com.allstate.spring.boot.restapi.entities.Customer;
import com.allstate.spring.boot.restapi.entities.Quote;
import com.allstate.spring.boot.restapi.repo.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class QuoteServiceImpl implements QuoteService {

    @Autowired
    private QuoteRepository quoteRepository;

    @Override
    public Collection<Quote> getAllQuotes() {
        return quoteRepository.findAll();
    }

    @Override
    public Optional<Quote> getQuoteByQuoteId(int quoteId) {
        return quoteRepository.findQuoteById(quoteId);
    }

    @Override
    public Collection<Quote> getQuotesByCustomerId(int customerId) {
        return quoteRepository.findQuotesByCustomerId(customerId);
    }

    @Override
    public Collection<Quote> getQuotesByCustomer(Customer customer) {
        return getQuotesByCustomerId(customer.getId());
    }

    @Override
    public Collection<Quote> getQuotesByEmail(String email) {
        return quoteRepository.findQuotesByEmail(email);
    }

    @Override
    public Collection<Quote> getQuotesByVin(String vin) {
        return quoteRepository.findQuotesByVin(vin);
    }

    @Override
    public Quote updateQuote(Quote updatedQuote) {
        return quoteRepository.save(updatedQuote);
    }

    @Override
    public void deleteQuote(Quote quote) {
        quoteRepository.delete(quote);
    }

    @Override
    public Quote createQuote(Quote newQuote) {
        return quoteRepository.save(newQuote);
    }
}
