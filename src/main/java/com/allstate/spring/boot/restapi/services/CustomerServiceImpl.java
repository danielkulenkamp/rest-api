package com.allstate.spring.boot.restapi.services;

import com.allstate.spring.boot.restapi.entities.Customer;
import com.allstate.spring.boot.restapi.repo.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Collection<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Optional<Customer> getCustomerById(int id) {
        return customerRepository.findById(id);
    }

    @Override
    public List<Customer> findCustomersByFirstName(String firstName) {
        return customerRepository.findCustomersByFirstName(firstName);
        // find all quotes that belong to that customer
    }

    @Override
    public Collection<Customer> findCustomersByLastName(String lastName) {
        return customerRepository.findCustomersByLastName(lastName);
    }

    @Override
    public Collection<Customer> findCustomersByFirstAndLastName(String firstName, String lastName) {
        return customerRepository.findCustomersByFirstNameAndLastName(firstName, lastName);
    }

    @Override
    public Collection<Customer> findCustomersByCity(String city) {
       return customerRepository.findCustomersByCity(city);
    }

    @Override
    public Collection<Customer> findCustomersByZipCode(String zip) {
        return customerRepository.findCustomersByZipCode(zip);
    }

    @Override
    public Customer updateCustomer(Customer updatedCustomer) {
        return customerRepository.save(updatedCustomer);
    }

    @Override
    public void deleteCustomer(Customer customer) {
        customerRepository.delete(customer);
    }

    @Override
    public Customer createCustomer(Customer newCustomer) {
        return customerRepository.save(newCustomer);
    }

}
