package com.allstate.spring.boot.restapi.services;

import com.allstate.spring.boot.restapi.entities.Customer;
import com.allstate.spring.boot.restapi.entities.Quote;

import java.util.Collection;
import java.util.Optional;

public interface QuoteService {

    Collection<Quote> getAllQuotes();
    Optional<Quote> getQuoteByQuoteId(int quoteId);
    Collection<Quote> getQuotesByCustomerId(int customerId);
    Collection<Quote> getQuotesByCustomer(Customer customer);
    Collection<Quote> getQuotesByEmail(String email);
    Collection<Quote> getQuotesByVin(String vin);

    Quote updateQuote(Quote updatedQuote);
    void deleteQuote(Quote quote);
    Quote createQuote(Quote newQuote);
}
