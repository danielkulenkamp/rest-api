package com.allstate.spring.boot.restapi.rest;

import com.allstate.spring.boot.restapi.entities.Customer;
import com.allstate.spring.boot.restapi.entities.Quote;
import com.allstate.spring.boot.restapi.services.CustomerService;
import com.allstate.spring.boot.restapi.services.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private QuoteService quoteService;

    @GetMapping("/customers/{customerId}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable("customerId") int id) {
        Optional<Customer> customer = customerService.getCustomerById(id);
        return handleResponse(customer);
    }

    private ResponseEntity<Customer> handleResponse(Optional<Customer> customer) {
        if (!customer.isPresent()) {
            return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Customer>(customer.get(), HttpStatus.OK);
        }
    }

    @GetMapping("/customers")
    public Collection<Customer> findBy(
            @RequestParam(value = "firstName", required = false) Optional<String> firstName,
            @RequestParam(value = "lastName", required = false) Optional<String> lastName,
            @RequestParam(value = "city", required = false) Optional<String> city,
            @RequestParam(value = "zip", required = false) Optional<String> zip
    ) {
        if (firstName.isPresent() && lastName.isPresent()) {
            return customerService.findCustomersByFirstAndLastName(firstName.get(), lastName.get());
        } else if (firstName.isPresent() && !lastName.isPresent()) {
            return customerService.findCustomersByFirstName(firstName.get());
        } else if (!firstName.isPresent() && lastName.isPresent()) {
            return customerService.findCustomersByLastName(lastName.get());
        } else if (city.isPresent()) {
            return customerService.findCustomersByCity(city.get());
        } else if (zip.isPresent()) {
            return customerService.findCustomersByZipCode(zip.get());
        } else {
            return customerService.getAllCustomers();
        }

    }

    @PutMapping("/customers/update")
    public Customer updateCustomer(@RequestBody Customer updatedCustomer) {
        return customerService.updateCustomer(updatedCustomer);
    }

    @DeleteMapping("/customers/delete/{customerId}")
    public ResponseEntity<Customer> deleteCustomerById(@PathVariable("customerId") int id) {
        Optional<Customer> customer = customerService.getCustomerById(id);
        return delete(customer);
    }

    private ResponseEntity<Customer> delete(Optional<Customer> customer) {
        if (!customer.isPresent()) {
            return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        } else {
            customerService.deleteCustomer(customer.get());
            return new ResponseEntity<Customer>(customer.get(), HttpStatus.OK);
        }
    }

    @DeleteMapping("/customers/delete")
    public ResponseEntity<Customer> deleteCustomer(@RequestBody Customer customerToDelete) {
        Optional<Customer> customer = customerService.getCustomerById(customerToDelete.getId());
        return delete(customer);
    }

    @PostMapping("/customers/add")
    public void addCustomer(@RequestBody Customer customer) {
        customerService.createCustomer(customer);
    }

    // #####################################################
    // Quotes API
    // #####################################################

//    @GetMapping("/quotes")
//    public Collection<Quote> getQuotes() { return quoteService.getAllQuotes(); }

    @GetMapping("/quotes/{quoteId}")
    public ResponseEntity<Quote> getQuoteById(@PathVariable("quoteId") int id) {
        Optional<Quote> quote = quoteService.getQuoteByQuoteId(id);
        return handleQuoteResponse(quote);
    }

    private ResponseEntity<Quote> handleQuoteResponse(Optional<Quote> quote) {
        if (!quote.isPresent()) {
            return new ResponseEntity<Quote>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Quote>(quote.get(), HttpStatus.OK);
        }
    }

    @GetMapping("/quotes")
    public Collection<Quote> findBy(
            @RequestParam(value = "email", required = false) Optional<String> email,
            @RequestParam(value = "vin", required = false) Optional<String> vin,
            @RequestParam(value = "customerId", required = false) Optional<Integer> customerId
    ) {
        if (customerId.isPresent()) {
            return quoteService.getQuotesByCustomerId(customerId.get());
        } else if (email.isPresent()) {
            return quoteService.getQuotesByEmail(email.get());
        } else if (vin.isPresent()) {
            return quoteService.getQuotesByVin(vin.get());
        } else {
            return quoteService.getAllQuotes();
        }
    }

    @PutMapping("/quotes/update")
    public Quote updateQuote(@RequestBody Quote updatedQuote) {
        return quoteService.updateQuote(updatedQuote);
    }

    @DeleteMapping("/quotes/delete/{quoteId}")
    public ResponseEntity<Quote> deleteQuoteById(@PathVariable("quoteId") int id) {
        Optional<Quote> quote = quoteService.getQuoteByQuoteId(id);
        return deleteQ(quote);
    }

    private ResponseEntity<Quote> deleteQ(Optional<Quote> quote) {
        if (!quote.isPresent()) {
            return new ResponseEntity<Quote>(HttpStatus.NOT_FOUND);
        } else {
            quoteService.deleteQuote(quote.get());
            return new ResponseEntity<Quote>(quote.get(), HttpStatus.OK);
        }
    }

    @DeleteMapping("/quotes/delete")
    public ResponseEntity<Quote> deleteQuote(@RequestBody Quote quoteToDelete) {
        Optional<Quote> quote = quoteService.getQuoteByQuoteId(quoteToDelete.getId());
        return deleteQ(quote);
    }

    @PostMapping("/quotes/add")
    public void addQuote(@RequestBody Quote quote) {
        if (quote.getDateCreated() == null) {
            quote.setDateCreated(LocalDate.now());
        }
        System.out.println(quote.getDateCreated());
        quoteService.createQuote(quote);
    }
}
